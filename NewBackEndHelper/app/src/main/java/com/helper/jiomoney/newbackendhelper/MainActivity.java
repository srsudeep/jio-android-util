package com.helper.jiomoney.newbackendhelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String HOST_NAME = "host_name";
    private static final String PORT_NO = "port_no";
    private static final String CLIENT_SECRET = "client_secret";
    private static final String JIO_CLIENT_ID = "client_id";
    private static final String MEP_CLIENT_ID = "mep_client_id";
    private static final String TRANSIT_CLIENT_ID = "transit_client_id";
    private static final String IS_PRE_POD = "is_pre_pod";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openTheJioMoney(View view) {

        String hostName = getViewValue(R.id.et_host_name);
        if (TextUtils.isEmpty(hostName)) {
            showMesssge("Please enter the host name");
            return;
        }
        String portNo = getViewValue(R.id.et_port_no);
       

        String clientSecret = getViewValue(R.id.et_client_secret);
        if (TextUtils.isEmpty(clientSecret)) {
            showMesssge("Please enter the client secret");
            return;
        }

        String walletId = getViewValue(R.id.et_wallet_client_id);
        if (TextUtils.isEmpty(walletId)) {
            showMesssge("Please enter the wallet client id");
            return;
        }

        String mepClientId = getViewValue(R.id.et_mep_client_id);
        if (TextUtils.isEmpty(mepClientId)) {
            showMesssge("Please enter the Mep Client Id");
            return;
        }

        String transitClientID = getViewValue(R.id.et_transit_client_id);
        if (TextUtils.isEmpty(transitClientID)) {
            showMesssge("Please enter the transit client id");
            return;
        }
        boolean isPrepod = ((CheckBox) findViewById(R.id.cb_prepod)).isChecked();

        Intent intent = new Intent("open.the.jiomoney.app");
        intent.putExtra(HOST_NAME, hostName);
        intent.putExtra(PORT_NO, portNo);
        intent.putExtra(CLIENT_SECRET, clientSecret);
        intent.putExtra(JIO_CLIENT_ID, walletId);
        intent.putExtra(MEP_CLIENT_ID, mepClientId);
        intent.putExtra(TRANSIT_CLIENT_ID, transitClientID);
        intent.putExtra(IS_PRE_POD, isPrepod);

        sendBroadcast(intent);
    }

    private String getViewValue(int id) {
        EditText etHostName = (EditText) findViewById(id);
        return etHostName.getText().toString();
    }

    private void showMesssge(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
