package com.jiomoney.jiomoneyhelper;

import java.io.Serializable;

/**
 * Created by AnkitGarg on 15/06/16.
 */
public class AppNamePackageModel implements Serializable {

    private String appName;
    private String packageName;
    private int hashCode;

    public AppNamePackageModel(String appName, String packageName) {
        this.appName = appName;
        this.packageName = packageName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public int hashCode() {
        hashCode = (this.appName + this.packageName).hashCode();
        return hashCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AppNamePackageModel{");
        sb.append("appName='").append(appName).append('\'');
        sb.append(", packageName='").append(packageName).append('\'');
        sb.append(", hashCode=").append(hashCode);
        sb.append('}');
        return sb.toString();
    }
}
