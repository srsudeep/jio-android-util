package com.jiomoney.jiomoneyhelper;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


public class LaunchActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUrl;
    private Button btnSubmit;
    private AppCompatSpinner spPackageSelector;
    private ArrayList<AppNamePackageModel> packageList = new ArrayList<>();
    private ArrayAdapter<AppNamePackageModel> adapter;
    private AppNamePackageModel selectedPackage;
    private Dialog dialog;
    private EditText etPackageName;
    private EditText etAppName;

    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedPackage = adapter.getItem(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        etUrl = (EditText) findViewById(R.id.etUrl);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        spPackageSelector = (AppCompatSpinner) findViewById(R.id.spPackageSelector);

        packageList.add(new AppNamePackageModel("JioMoney", "com.corpay.mwallet"));
        packageList.add(new AppNamePackageModel("JioCoupons", "com.jiomoney.jiocoupons"));

        adapter = new PackageAdapter(this,
                android.R.layout.simple_dropdown_item_1line,
                packageList);

        spPackageSelector.setAdapter(adapter);
        spPackageSelector.setOnItemSelectedListener(onItemSelectedListener);

        etUrl.setSelection(etUrl.getText().toString().length());
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                try {
                    Intent intent = new Intent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setData(Uri.parse("jiomoney://" + etUrl.getText().toString().trim()));
                    intent.setPackage(selectedPackage.getPackageName());
                    intent.setType("text/url");
                    intent.setComponent(new ComponentName("com.corpay.mwallet","com.corpay.mwallet.app.ui.flows.login.AppLoginActivity"));
                    intent.putExtra(Intent.EXTRA_TEXT, etUrl.getText().toString().trim());
                    intent.putExtra(Intent.EXTRA_CC,getSHA());
                    startActivity(intent);
                } catch (Exception e) {
                    Snackbar.make(findViewById(R.id.rlContainer),
                            getString(R.string.error_opening_package, new String[] {e.getMessage()}),
                            Snackbar.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnAdd:
                String appName = etAppName.getText().toString().trim();
                String packageName = etPackageName.getText().toString().trim();
                if (appName.length() > 0
                        && packageName.length() > 0) {

                    packageList.add(new AppNamePackageModel(appName, packageName));
                    adapter.notifyDataSetChanged();

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                } else {
                    Snackbar.make(findViewById(R.id.rlContainer), "App/Package name cannot be empty", Snackbar.LENGTH_SHORT).show();
                }

                break;

            case R.id.btnCancel:
                if (dialog != null) {
                    dialog.dismiss();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add) {
            showAddPackageDialog();
        }
        if (item.getItemId() == android.R.id.home)
            finish();
        return true;
    }

    private void showAddPackageDialog() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_add_package);

        etAppName = (EditText) dialog.findViewById(R.id.etAppName);
        etAppName.setSelection(etAppName.getText().toString().length());

        etPackageName = (EditText) dialog.findViewById(R.id.etPackageName);
        etPackageName.setSelection(etPackageName.getText().toString().length());

        Button btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);

        if (!this.isFinishing() && !dialog.isShowing()) {

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }


    }

    private String getSHA(){
        PackageInfo info;
        String hashKey=null;
        try {

            info = getPackageManager().getPackageInfo(
                    "com.corpay.mwallet", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                hashKey = new String(Base64.encode(md.digest(), 0));

            }

        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        return hashKey;
    }


}
