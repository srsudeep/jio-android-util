package com.jiomoney.jiomoneyhelper;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by AnkitGarg on 15/06/16.
 */
public class ApplicationController extends Application {

    private static ApplicationController mInstance;
    private SQLiteManager sqLiteManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        mInstance = this;
        sqLiteManager = new SQLiteManager(this);

    }

    public static ApplicationController getInstance() {
        return mInstance;
    }


    public SQLiteManager getSqLiteManager() {
        return sqLiteManager;
    }


}
