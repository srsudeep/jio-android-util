package com.jiomoney.jiomoneyhelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AnkitGarg on 15/06/16.
 */
public class PackageAdapter extends ArrayAdapter<AppNamePackageModel> {
    private Context mContext;
    private int mResId;
    private ArrayList<AppNamePackageModel> mItems;
    private LayoutInflater mInflater;

    public PackageAdapter(Context context, int resource, ArrayList<AppNamePackageModel> objects) {
        super(context, resource, objects);
        mContext = context;
        mResId = resource;
        mItems = objects;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mResId, parent, false);

        AppNamePackageModel packageModel = mItems.get(position);
        TextView tvLabel = (TextView) convertView.findViewById(android.R.id.text1);
        tvLabel.setText(packageModel.getAppName() + " - " + packageModel.getPackageName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);

        AppNamePackageModel packageModel = mItems.get(position);
        TextView label = (TextView) convertView.findViewById(android.R.id.text1);
        label.setText(packageModel.getAppName() + " - " + packageModel.getPackageName());

        return convertView;
    }
}
